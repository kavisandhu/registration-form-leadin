<?php
/**
 * Template Name: 2-Column (Dashboard)
 *
 * A custom page template without sidebar.
 *
 * The "Template Name:" bit above allows this to be selectable
 * from a dropdown menu on the edit page screen.
 *
 * @package WordPress
 * @subpackage Twenty_Ten
 * @since Twenty Ten 1.0
 */

get_header(); ?>
<?php
$blog_id = 2;
switch_to_blog($blog_id);
$table = $wpdb->get_blog_prefix($blog_id) . 'li_submissions';
$hashkey = $_COOKIE['li_hash'];
$fields = array(
			array('label'=>'email','value'=>'tested@test.com'),
			array('label'=>'name','value'=>'test name')
			);
$fields = json_encode($fields);

$data = array(
			'lead_hashkey'=> $hashkey,
			'form_page_title' => 'Test form',
			'form_page_url' => 'http://localhost/kripalu/site1/2015/03/03/hello-world/',
			'form_fields' => $fields,
			'form_selector_id' => 'subscribeform',
			'form_selector_classes' => 'subscribe_form',
			'form_hashkey' => $hashkey,
			'blog_id' => $blog_id
			);
$wpdb->insert($table, $data);

$table = $wpdb->get_blog_prefix($blog_id) . 'li_leads';
$email = 'test@test.com';
$data = array(
			'hashkey'=> $hashkey,
			'lead_ip' => $_SERVER["REMOTE_ADDR"],
			'lead_email' => $email,
			'lead_first_name' => '',
			'lead_last_name' => '',
			'lead_status' => 'subscribe',
			'blog_id' => $blog_id
			);
			
$wpdb->insert($table, $data);

restore_current_blog();
?>
<?php get_footer(); ?>
